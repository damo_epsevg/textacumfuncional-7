package edu.upc.damo;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Josep M on 09/07/2015.
 */
public class GestorTeclat{
    public static void amagaTeclat(Context c,View v) {
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static  void mostraTeclat(Context c, View v) {
        amagaTeclat(c, v);    // Neccessari, ja que no sé per quins set sous, sense ell el primer cop no xuta
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v,InputMethodManager.RESULT_SHOWN);
    }


    public static boolean hiHaTeclatHard(Context c) {
        return c.getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }

    public static Teclat  getInstanceTeclat(Context c){
        if (hiHaTeclatHard(c))
            return new TeclatHard();
        else
            return new TeclatSoft();
    }


    public  static abstract class Teclat {
        public final static int  DESCONEGUT = 0;
        public final static int  OK = 1;

        abstract public int accio( int actionId, KeyEvent event);

    }

    private static class TeclatHard extends Teclat{
        @Override
        public int accio(int actionId, KeyEvent event) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_UP:
                    return OK;
            }
            return DESCONEGUT;
        }
    }
    private static class TeclatSoft extends Teclat {
        @Override
        public int accio(int actionId, KeyEvent event) {
            switch (actionId) {
                case EditorInfo.IME_ACTION_GO:
                    return OK;
            }
            return DESCONEGUT;
        }
    }
}